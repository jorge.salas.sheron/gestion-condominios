package Programa;

public class Condominio {
    String nombrecondominio;
    String direccion;
    String codcondominio;

    public Condominio(String nombrecondominio, String direccion, String codcondominio) {
        this.nombrecondominio = nombrecondominio;
        this.direccion = direccion;
        this.codcondominio = codcondominio;
    }
    public Condominio (String codcondominio) {}

    public Condominio () {}

    public String getNombrecondominio() {
        return nombrecondominio;
    }

    public void setNombrecondominio(String nombrecondominio) {
        this.nombrecondominio = nombrecondominio;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCodcondominio() {
        return codcondominio;
    }

    public void setCodcondominio(String codcondominio) {
        this.codcondominio = codcondominio;
    }

    @Override
    public String toString() {
        return "{" +
                "nombrecondominio='" + nombrecondominio + '\'' +
                ", direccion='" + direccion + '\'' +
                ", codcondominio='" + codcondominio + '\'' +
                '}';
    }
}

