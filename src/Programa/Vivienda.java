package Programa;

public class Vivienda extends Condominio{
    String sector; //la torre o el bloque donde esta el departamento
    String numvivienda; // numero de departamento
    String codvivienda;
    String areavivienda;
    boolean cochera;

    public Vivienda(String nombrecondominio, String direccion, String codcondominio, String sector, String numvivienda, String codvivienda, String areavivienda, boolean cochera) {
        super(nombrecondominio, direccion, codcondominio);
        this.sector = sector;
        this.numvivienda = numvivienda;
        this.codvivienda = codvivienda;
        this.areavivienda = areavivienda;
        this.cochera = cochera;
    }
    public Vivienda(String codcondominio, String codvivienda) {
        super(codcondominio);
        this.codvivienda = codvivienda;
    }
    public Vivienda() {}

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getNumvivienda() {
        return numvivienda;
    }

    public void setNumvivienda(String numvivienda) {
        this.numvivienda = numvivienda;
    }

    public String getCodvivienda() {
        return codvivienda;
    }

    public void setCodvivienda(String codvivienda) {
        this.codvivienda = codvivienda;
    }

    public String getAreavivienda() {
        return areavivienda;
    }

    public void setAreavivienda(String areavivienda) {
        this.areavivienda = areavivienda;
    }

    public boolean isCochera() {
        return cochera;
    }

    public void setCochera(boolean cochera) {
        this.cochera = cochera;
    }

    @Override
    public String toString() {
        return "Vivienda{" +
                "codcondominio='" + codcondominio + '\'' +
                ", codvivienda='" + codvivienda + '\'' +
                ", areavivienda='" + areavivienda + '\'' +
                ", cochera=" + cochera +
                '}';
    }
}
